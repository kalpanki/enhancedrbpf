# README #

This is an implementation of the paper on Dynamic Reverse Analysis for background subtraction and tracking.

# Steps to Follow #

- Checkout the code from this repository
- Download the car sequence and the PETs sequence to the same directory.
- To run just the GMM, run the file `run_gray.m`.
- To run DRA / bidirectional GMM approach, run the file, `run.m`. 
- By default it assumes a trained model is saved and available in the same directory. If not, set the `loadSavedData` value to 0 and train with your data.