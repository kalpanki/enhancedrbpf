clc; clear all; close all;

% some constants to update the GMM model
alpha = 0.1;
gmmK = 5;
numTrainFrames = 500;
start = numTrainFrames + 1;
T = 200; % threshold btw fg and bg

dataset = 'highway';

imgSeq = readImgSeq(dataset);
disp('Done with reading the image sequence');

% Using the trained models initialize and run GMM bg estimation
mix_gauss( imgSeq , dataset, gmmK, alpha, T);



                


