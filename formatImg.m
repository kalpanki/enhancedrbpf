function [res] = formatImg(image)
% Given an RGB image, converts to R,G,B channel matrix, res

    vectR = []; vectG = []; vectB = [];
    chR = image(:,:,1); chR = chR(:);
    chG = image(:,:,2); chG = chG(:);
    chB = image(:,:,3); chB = chB(:);
    vectR = [vectR ; chR];
    vectG = [vectG ; chG];
    vectB = [vectB ; chB];
    
    res = double([vectR vectG vectB]);

end