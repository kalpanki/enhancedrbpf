clc; close all; clear all;

% some parameters 
numTrainFrames = 300;
gmmK = 5;
start = numTrainFrames + 1;
dataset = 'pets';
% should we used saved trained model instead of trainung again?
loadSavedData = 1; 

% load the dataset
imgSeq =  readImgSeq(dataset);
disp('done with loading the data');

% for similarity measure
similarityK = 20;
weight1 = 0.9;
weight2 = 0.1;

% some constants to update the GMM model
alpha = 0.001;
rho = 0.01;

% weights threshold to differentiate bg and fg
fwd_T1 = 0.25;
rev_T1 = 0.3;
mahalThresh = chi2inv(0.95, 3);


%% Train and generate models first 

if (loadSavedData == 0)
    % train with the forward frames
    fwdModel = trainGmm(imgSeq, numTrainFrames, gmmK, start, 'forward');
    if (strcmp(dataset,'pets'))
        save('fwdModel_pets','fwdModel');
    else
        save('fwdModel_car','fwdModel');
    end
    disp('Done with training the forward model');
    % train with backward frames
    revModel = trainGmm(imgSeq, numTrainFrames, gmmK, start, 'reverse');
    if (strcmp(dataset,'pets'))
        save('revModel_pets','revModel');
    else
        save('revModel_car','revModel');
    end
    disp('Done with training the reverse model');
else 
    % load trained data
    fwdModel = load('fwdModel_pets');
    fwdModel = fwdModel.fwdModel;
    revModel = load('revModel_pets');
    revModel = revModel.revModel;
end

%% Test on the rest of the frames

% image I(t)
Yt = formatImg((imgSeq(:,:,:,start)));
% lets initialize our model with learnt params
% weights, % mean - mu(t), 
revModelWeights = revModel.ComponentProportion;
fwdModelWeights = fwdModel.ComponentProportion;
revMahalDist = mahal(revModel, Yt);
fwdMahalDist = mahal(fwdModel, Yt);
[dummy1, revMatchKVals] = min(revMahalDist');
[dummy1, fwdMatchKVals] = min(fwdMahalDist');
% mean and sigma for each pixel
[revModelMeans,revModelSigmas] = meanSigmaForPixels(Yt, gmmK , revMatchKVals, ...
                                            revModel.mu, revModel.Sigma);
[fwdModelMeans,fwdModelSigmas] = meanSigmaForPixels(Yt, gmmK , fwdMatchKVals, ...
                                            fwdModel.mu, fwdModel.Sigma);
% co-variance matrix - sigma(t)
% modelSigma = sigmaForPixels(Yt, gmmK, matchKVals, reverse_model.Sigma, modelMean);



%% Model update step

% to store all frames for similarity measure
kk = 1;
figure(1); 
tic;

for j = 300:302 % image I(t+1)
    
    fprintf('in for loop, running for frame No.: %d, \n', j);
    % get test image
    testImg = (imgSeq(:,:,:,j));
    subplot(131), imshow(uint8(testImg),[]); title('Given image');
    
    [nRows,nCols,chs] = size(testImg);
    % our final mask
    revFgMask = zeros(nRows,nCols);
    fwdFgMask = zeros(nRows,nCols);
    
    % format the test image for classifying
    % Y is X(t+1) in equation
    Y = formatImg(testImg);
    
    % get Mahalanobis distance for all pixels
    revMahalDist = mahal(revModel, Y);
    fwdMahalDist = mahal(fwdModel, Y);
    
    % get nearest gaussian K for each pixel
    [dummy1, revMatchKVals] = min(revMahalDist');
    revBestK = reshape(revMatchKVals,[nRows,nCols]);
    [dummy1, fwdMatchKVals] = min(fwdMahalDist');
    fwdBestK = reshape(fwdMatchKVals,[nRows,nCols]);
    
    revFgKs = find(revModelWeights < rev_T1);
    fwdFgKs = find(fwdModelWeights < fwd_T1);
    
    for k = 1:length(revFgKs)
        revFgMask = revFgMask + (revBestK == revFgKs(k));
    end
    for k = 1:length(fwdFgKs)
        fwdFgMask = fwdFgMask + (fwdBestK == fwdFgKs(k));
    end
    
    % update the model parameters
    % weightsc
    revModelWeights = ((1-alpha) * revModelWeights) + alpha;
    revModelWeights = revModelWeights ./ sum(revModelWeights);
    fwdModelWeights = ((1-alpha) * fwdModelWeights) + alpha;
    fwdModelWeights = fwdModelWeights ./ sum(fwdModelWeights);
    
    fprintf('Rev model weights: %d , fwd model weights: %d \n', revModelWeights, fwdModelWeights);
    
    % means for each pixel
    revModelMeans = ((1 - alpha) * revModelMeans) + (rho * Y);
    fwdModelMeans = ((1 - alpha) * fwdModelMeans) + (rho * Y);

    for ii = 1:length(Y)
        % our new sigmal val
        revDiagSigma = diag(revModelSigmas(ii,:));
        %revDiagSigma = revModelSigmas{ii};
        revModelSigma = ((1-rho)*revDiagSigma) + (rho * (Y(ii,:) - revModelMeans(ii,:)) * (Y(ii,:) - revModelMeans(ii,:))');
        fwdDiagSigma = diag(fwdModelSigmas(ii,:));
        %fwdDiagSigma = fwdModelSigmas{ii};
        fwdModelSigma = ((1-rho)*fwdDiagSigma) + (rho * (Y(ii,:) - fwdModelMeans(ii,:)) * (Y(ii,:) - fwdModelMeans(ii,:))');
        % get the matching distribution
        revMatchingK = revMatchKVals(ii);
        fwdMatchingK = fwdMatchKVals(ii);
        
        % update our GMM model with new mean and sigma
        muTemp = revModel.mu;
        sigmaTemp = revModel.Sigma;
        muTemp(revMatchingK,:) = revModelMeans(ii,:);
        sigmaTemp(:,:,revMatchingK) = diag(revModelSigma)';
        %sigmaTemp(:,:,revMatchingK) = revModelSigma;
        revModel = gmdistribution(muTemp, sigmaTemp, revModelWeights);
        
        muTemp = fwdModel.mu;
        sigmaTemp = fwdModel.Sigma;
        muTemp(fwdMatchingK,:) = fwdModelMeans(ii,:);
        sigmaTemp(:,:,fwdMatchingK) = diag(fwdModelSigma)';
        %sigmaTemp(:,:,fwdMatchingK) = fwdModelSigma';
        fwdModel = gmdistribution(muTemp, sigmaTemp, fwdModelWeights);
    end
    
    subplot(132); imshow(revFgMask,[]); title('Reverse Model'); 
    subplot(133); imshow(fwdFgMask,[]); title('Forward Model');
    pause(0.5);
    
    revFgMasks{kk} = revFgMask;
    % we really need it for similarity. so just sum channels
    revFgFrames{kk} = revFgMask .* (sum(testImg,3));
    fwdFgMasks{kk} = fwdFgMask;
    fwdFgFrames{kk} = fwdFgMask .* (sum(testImg,3));
    
    kk = kk+1;
    toc;
end % end of frames

%% Similarity measure 
% using the stored Forward frames, we need to compute similarity

%difference between foreground detected outputs
% for i = 1 : length(fwdFgFrames)
%    diff_frames{i} = weight1 * (fwdFgFrames{i} - revFgFrames{i});
% end

for i = 1 :length(fwdFgFrames) % or revFgFrames
    rev_count = sum(sum(revFgFrames{i}));
    fwd_count = sum(sum(fwdFgFrames{i}));
    %diff{i} = weight1 * (fwdFgFrames{i} - revFgFrames{i});
    int_diff(i) = weight1 * abs(rev_count - fwd_count);
end

for i = 1:length(revFgMasks)
   rev_mask_cnt = sum(sum(revFgMasks{i}));
   fwd_mask_cnt = sum(sum(fwdFgMasks{i}));
   mask_diff(i) = abs(rev_mask_cnt - fwd_mask_cnt);
end

% normalize so that we can apply threshold
int_diff = int_diff / max(int_diff);
mask_diff = mask_diff / max(mask_diff);
plot(mask_diff,'b--'); hold on;

%% Curve fitting to fit the above similarity


kappa = polyfit(1:51,int_diff,6);
fittingVals = polyval(kappa,1:51);
plot(1:51, fittingVals,'r-');
legend('Real Difference', 'Fitted Function');

