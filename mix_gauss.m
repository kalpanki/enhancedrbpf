function [ ] = mix_gauss( img_seq, dataset, num_gauss, alpha, T)
%MIX_GAUSS Mixture of Gaussians for Background estimation
%   d, the number of channels in sequence is assumed to be 1 for
%   simplicity.

    [d1 d2 d3] = size(img_seq);
    % number of distributions that form the background model
    b = 3; 
    % init each pixel has a guassian
    I_0 = img_seq(:,:,1);
    [m n] = size(I_0);
    for k = 1:num_gauss
        mu(:,:,k) = I_0 .* ones(m,n);
        sigma(:,:,k) = 2.* ones(m,n);
        w(:,:,k) = ones(m,n).*(1/num_gauss);
    end
    
    if (strcmp(dataset,'highway'))
        start = 470;
        %gt_seq = read_gt(dataset);
    else
        start = 1;
    end
    
    % estimate background for each image
    figure(1);
    for f = start:d3
        I = img_seq(:,:,f);
        % match gaussians
        for k = 1:num_gauss
            int_diff(:,:,k) = abs(I - mu(:,:,k));
            upd_mask(:,:,k) = int_diff(:,:,k) < (2.5.*sigma(:,:,k));
            Mk = upd_mask(:,:,k);
            rho = alpha .* I;
            % update weights and parameters
            mu(:,:,k) = rho.*Mk.*I + (1-alpha).*Mk.*mu(:,:,k);
            sigma(:,:,k) = rho.*(I-mu(:,:,k)).*(I-mu(:,:,k)) + ...
                    (1-rho).*sigma(:,:,k).*sigma(:,:,k);
        end
        
        % if none of the K pdfs match, replace least probable
        % distribution with current intensity value
        mu_to_update = sum(upd_mask,3) == 0;
        % find which K should be used to update
        [vals max_idxs] = max(int_diff,[],3);
        for i = 1:num_gauss
            upd_mask(:,:,i) = max_idxs == i;
            mu(:,:,i) = mu(:,:,i).*upd_mask(:,:,i) ...
                        + mu_to_update .* I ;
        end
        
        % update weights and normalize
        for k = 1:num_gauss
            w(:,:,k) = alpha.*Mk + (1-alpha).*w(:,:,k);
            w(:,:,k) = w(:,:,k) ./ sum(w,3);
        end
        
        % compute the background using weights and variance
        % the background is something that has minimum
        temp = w./sigma;
        [dummy gauss_idxs] = sort(sum(sum(temp)));
        % choose the first b dustributions as background model
        mean_img = mean(mu(:,:,gauss_idxs(1:b)),3);
        bg_est = mean_img < T;
        fg_est = bg_est == 0;
        
        figure(1); subplot(121); imshow(I,[]); title('Input Frames');
        figure(1); subplot(122); imshow(fg_est,[]); title('Reverse Model');
        pause(0.1);
        
        % get the centroid for rectangle
        cen = regionprops(fg_est, 'centroid');
        centers = cat(1,cen.Centroid);
        ma = regionprops(fg_est, 'MajorAxisLength');
        ma = cat(1,ma.MajorAxisLength);
        mi = regionprops(fg_est, 'MinorAxisLength');
        mi = cat(1,mi.MinorAxisLength);
        for i = 1:length(mi)
            rectangle('Position',[centers(i,1)-ma(i)/2,centers(i,2)-mi(i)/2, ...
                ma(i), mi(i)], 'EdgeColor','r');
        end
        
    end % for each frame

    close all;
end
