function [meanVals,sigmaVals] = meanForPixels(pixVals, gmmK, matchKVals, modelMean, modelSigma)
% pixVals - A matrix of size (no. of Pix * 3), 3 - no. of channels


    for i = 1:length(pixVals)
        for k = 1:gmmK
            if (k == matchKVals(i))
                meanVals(i,:) = modelMean(k,:);
                % diagonals of cov matrix
                sigmaVals(i,:) = modelSigma(:,:,k);
                %sigmaVals{i} = modelSigma(:,:,k);
            end
        end
    end

end