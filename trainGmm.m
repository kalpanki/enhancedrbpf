function [obj] = trainGmm( imgSeq, N, K , start, mode)
% N - Number of frames to use for training
% K - Number of Gaussians in the mixture 

vectR = [];
vectG = [];
vectB = [];
X = [];

% choose training frames
if (strcmp(mode, 'forward'))
    currSeq = imgSeq(:,:,:,start:(start+N));
else
    currSeq = imgSeq(:,:,:,start:-1:(start-N));
end

% format data - split images into R G B channels
for i = 1:N
   currImg = imresize(currSeq(:,:,:,i), 0.5); 
   tempX = formatImg(currImg);
   X = [X ; tempX];
end

options = statset('Display','off','MaxIter',1000);
obj = gmdistribution.fit(double(X), K, ...
                    'Options',options, ...
                    'Regularize',0.001, ...
                    'Start', 'randSample', ...
                    'CovarianceType', 'diag', ...
                    'Replicates', 1);

end