function [ gt_seq ] = readImgSeq(dataset)
%READ_GT Reads the ground truth dataset and returns it as sequence


    % read the ground truth data for comparison
    if(strcmp(dataset,'highway'))
        imPath = 'highway'; imExt = 'jpg';
        filearray = dir([imPath filesep '*.' imExt]); 
        NumImages = size(filearray,1); 
    end
    if (strcmp(dataset,'pets'))
        imPath = 'pets2001_ds1'; imExt = 'jpg';
        filearray = dir([imPath filesep '*.' imExt]);
        NumImages = 700;
    end
    
    % read all of them
    for i=1:NumImages
        img_name = [imPath filesep filearray(i).name];
        gt_seq(:,:,:,i) = double(imresize( imread(img_name), 0.5));
    end
    
end

